#!/usr/bin/env python3

# -------------------------------
# projects/Diplomacy/TestDiplomacy.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = StringIO("A Madrid Hold\n")
        i = diplomacy_read(s)
        self.assertEqual(i, ['A Madrid Hold'])
    
    def test_read_2(self):
        s = StringIO("A Madrid Hold\nB Barcelona Move Madrid\n")
        i = diplomacy_read(s)
        self.assertEqual(i, ['A Madrid Hold', 'B Barcelona Move Madrid'])
    
    def test_read_3(self):
        s = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        i = diplomacy_read(s)
        self.assertEqual(i, ['A Madrid Hold', 'B Barcelona Move Madrid', 'C London Support B'])

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = diplomacy_eval(['A Madrid Hold', 'B Barcelona Move Madrid'])
        self.assertEqual(v, {'A':'[dead]', 'B':'[dead]'})
    
    def test_eval_2(self):
        v = diplomacy_eval(['A Madrid Hold', 'B Barcelona Move Madrid', 'C London Move Madrid'])
        self.assertEqual(v, {'A':'[dead]', 'B':'[dead]', 'C':'[dead]'})

    def test_eval_3(self):
        v = diplomacy_eval(['A Madrid Hold', 'B Barcelona Move Madrid', 'C London Move Madrid', 'D Paris Support B'])
        self.assertEqual(v, {'A':'[dead]', 'B':'Madrid', 'C': '[dead]', 'D':'Paris'})

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        diplomacy_print(w, {'A':'Madrid'})
        self.assertEqual(w.getvalue(), "A Madrid\n")
    
    def test_print_2(self):
        w = StringIO()
        diplomacy_print(w, {'A':'[dead]', 'B':'Madrid', 'C':'[dead]', 'D':'Paris'})
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\n")

    def test_print_3(self):
        w = StringIO()
        diplomacy_print(w, {'A':'[dead]', 'B':'[dead]', 'C':'[dead]', 'D':'Paris', 'E':'Austin'})
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\n")

    # Also: Diplomacy_solver1.py: line 52 to exit
    def test_solve_2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    def test_solve_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move London\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\nB London\n")  

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1


$ cat TestDiplomacy.out
............
----------------------------------------------------------------------
Ran 12 tests in 0.000s

OK

$ coverage report -m                   >> TestDiplomacy.out

$ cat TestDiplomacy.out
............
----------------------------------------------------------------------
Ran 12 tests in 0.000s

OK
Name                   Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------------
Diplomacy.py              28      0     14      0   100%
Diplomacy_solver1.py      59      0     31      0   100%
TestDiplomacy.py          54      0      0      0   100%
------------------------------------------------------------------
TOTAL                    141      0     45      0   100%


"""